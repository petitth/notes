# Links

## Design Tools

| Name                                | Link                                           |
|-------------------------------------|------------------------------------------------|
| diagrams                            | https://app.diagrams.net                       |
| Online Paint                        | https://jspaint.app                            |
| yEd graph editor                    | https://www.yworks.com/products/yed/download   |
| Pencil project (prototyping tool)   | https://pencil.evolus.vn/                      |
| Wireframe Sketcher (screen mockups) | https://wireframesketcher.com/                 |
| Violet UML editor                   | http://alexdp.free.fr/violetumleditor/page.php |

## Coding related Tools

| Name                  | Link                             |
|-----------------------|----------------------------------|
| multi-database client | https://dbeaver.io               |
| MongoDb client        | https://www.nosqlbooster.com     |
| SmartGit              | https://www.syntevo.com/smartgit |

## Dev utilities

| Name             | Link                                                   |
|------------------|--------------------------------------------------------|
| Lorem Ipsum      | https://www.lipsum.com/feed/html                       |
| Color codes list | https://fr.wikipedia.org/wiki/Liste_de_noms_de_couleur |

## Misc

| Name                 | Link                                       |
|----------------------|--------------------------------------------|
| Calendar to download | https://icalendrier.fr                     |
| Mardown Cheat Sheet  | https://www.markdownguide.org/cheat-sheet/ |

